package demo.registerPage.day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AutomationTesting {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\SH40064174\\Downloads\\Softwares\\chromedriver_win32\\chromedriver.exe");
		WebDriver chrome = new ChromeDriver();
		chrome.manage().window().maximize();
		chrome.get("http://demo.automationtesting.in/Register.html");

		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[1]/input")).sendKeys("Abc"); // firstname
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[2]/input")).sendKeys("xyz"); // lastname
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[2]/div/textarea")).sendKeys("Address1"); // address
		chrome.findElement(By.xpath("//*[@id='eid']/input")).sendKeys("abc.xyz@gmail.com"); // email
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[4]/div/input")).sendKeys("1234567890"); // phone no.
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[5]/div/label[2]/input")).click(); // radio button
		chrome.findElement(By.xpath("//*[@id='checkbox1']")).click(); // check box 1
		chrome.findElement(By.xpath("//*[@id='checkbox2']")).click(); // check box 2

		chrome.findElement(By.xpath("//*[@id='msdd']")).click(); // click on ->multi-select drop-down
		
		//choose some languages by clicking on them
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[7]/div/multi-select/div[2]/ul/li[2]/a")).click();		
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[7]/div/multi-select/div[2]/ul/li[16]/a")).click();	
		//cancel some languages
		chrome.findElement(By.xpath("//*[@id='msdd']/div/span")).click();	
		
		//selecting drop-down - skills
		new Select(chrome.findElement(By.xpath("//*[@id='Skills']"))).selectByValue("Adobe InDesign");;
		new Select(chrome.findElement(By.xpath("//*[@id='Skills']"))).selectByValue("Certifications");
		
		//selecting drop-down - country
		new Select(chrome.findElement(By.xpath("//*[@id='countries']"))).selectByValue("India");
		
		//click and search drop-down
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[10]/div/span/span[1]/span")).click();
		//write on the search bar
		chrome.findElement(By.className("select2-search__field")).sendKeys("In");
		//select on the desired option
		chrome.findElement(By.xpath("//*[@id='select2-country-results']/li")).click();
		
		//Calendar-drop down - yr
		chrome.findElement(By.xpath("//*[@id='yearbox']")).click();
		new Select(chrome.findElement(By.xpath("//*[@id='yearbox']"))).selectByValue("1947");
		//-month
		chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[11]/div[2]/select")).click();
		new Select(chrome.findElement(By.xpath("//*[@id='basicBootstrapForm']/div[11]/div[2]/select"))).selectByValue("January");
		//-day
		chrome.findElement(By.xpath("//*[@id='daybox']")).click();
		new Select(chrome.findElement(By.xpath("//*[@id='daybox']"))).selectByValue("16");
		
		//password
		chrome.findElement(By.xpath("//*[@id='firstpassword']")).sendKeys("Aabc1Aabc1");;
		//conf.password
		chrome.findElement(By.xpath("//*[@id='secondpassword']")).sendKeys("Aabc1Aabc1");;
		
		
		chrome.findElement(By.xpath("//*[@id='submitbtn']")).click(); // submit
		chrome.findElement(By.xpath("//*[@id='Button1']")).click(); // refresh
		
	}

}
