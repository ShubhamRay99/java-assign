package demo.practice.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GettingLinks {

	public static void main(String[] args) {
//		System.setProperty("webdriver.chrome.driver","C:\\Users\\SH40064174\\Downloads\\Softwares\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver","C:\\Users\\SH40064174\\Downloads\\Softwares\\working\\chromedriver_win32\\chromedriver.exe");

		WebDriver chrome = new ChromeDriver();
		chrome.get("https://www.bbc.com/news");
		chrome.manage().window().maximize();
		
		List<WebElement> linksList = chrome.findElements(By.tagName("a"));
		linksList.forEach(link ->{
			System.out.println(link.getText());
		});
		
	}

}
