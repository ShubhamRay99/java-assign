package demo.practice.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelectingBasics {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\SH40064174\\Downloads\\Softwares\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.yahoo.com");
		
		System.out.println(driver.findElement(By.id("header-signin-link")).getText()); //prints sign-in
		
//		let suppose, there be a situation <input type="submit" value="sign-in"></input>
//		in this case we have to get the text from the attribute.
//		here, we can use, driver.findElement(By.xpath("..")).getAttribute(..);
		
		driver.close();
	}
}
